# A CMM compiler test suite.

written by _Naphtali Levy_.

## Instructions
Link your compiler (or copy it over) to the root directory of this tester (where `test.sh` is located).

If needed, also add (or link) the runtime files (linker and vm).

Run:
```sh
./test.sh
```
