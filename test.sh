#!/bin/bash
TEST_ROOT="testsuit"
if [[ $1 == "clean" ]] ; then
	echo -e "\033[0;34mcleaning test directory...\033[m";
	testdirs=`ls $TEST_ROOT`
	for dir in ${testdirs[@]}
	do
		files=`ls $TEST_ROOT/$dir`
		for file in ${files[@]}
		do
			ext=`echo "$file" | cut -f2 -d"."`
			if [[ $ext != "cmm" ]] && [[ $ext != "exp" ]] && [[ $ext != "input" ]] && [[ $ext != "lst" ]] && [[ $ext != "okerr" ]]
			then
				echo -e "\033[0;33mremoved:\033[m $TEST_ROOT/$dir/$file";
				rm $TEST_ROOT/$dir/$file
			fi
		done
	done
	exit
fi

if [[ $# > 0 ]] && [[ $1 != "update" ]] ; then
	printf "\033[0;33mUnknown argument passed to script -- script usage:\033[m\n";
	printf "\033[4;33mclean\033[m -- cleans the test environment from all files which are not .cmm, .exp, .input, .lst or .okerr extensions -- without running the test\n"
	printf "\033[4;33mupdate\033[m -- runs tests as normal, and if there are any conflicts with output and expected, will overwrite expected. Will print UPDATED! in test status\n"
	exit
fi


printf "\n\n▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓\n"
printf "▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒\n"
printf "░░░░░░░░   \e[1;35mBeginning test suite! -- GOOD LUCK!\033[m\t ░░░░░░░░░░\n"
printf "▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒\n"
printf "▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓\n\n"

total_comp_errors=0;
total_comp=0;
total_link_errors=0;
total_link=0;
total_test_errors=0;
total_test=0;

pad=$(printf '%0.1s' "."{1..70})
padlength=70

testdirs=`ls $TEST_ROOT`
for dir in ${testdirs[@]}
do
	echo
	echo =======================================
	echo -e "===\t\e[1;35mBeginning $dir test\033[m\t ======"
	echo =======================================

	#first clean the area...
	`rm -f $TEST_ROOT/"$dir"/*.e`
	`rm -f $TEST_ROOT/"$dir"/*.rsk`
	`rm -f $TEST_ROOT/"$dir"/*.out`

	sourcefiles=`ls $TEST_ROOT/"$dir"/*.cmm`
	for source in ${sourcefiles[@]}
	do
		#printf "compiling  $source  \t.......................";
		printf '%s%*.*s' "compiling $source " 0 $((padlength - ${#source} - 1 )) "$pad";
		cmd=`printf './rx-cc %s' "$source"`
		total_comp=$(($total_comp+1));
		ERROR=$($cmd 2>&1)
		error_type=$?
		errordif=$error_type
		if [[ $error_type -ne 0 ]] ; then
			printf '%s\n' "COMPILER ERROR - Error type: $error_type" > $TEST_ROOT/"$dir"/compiler.err
			errordif=1
			if [[ -e $TEST_ROOT/"$dir"/compiler.okerr ]] ; then
				`diff $TEST_ROOT/"$dir"/compiler.okerr $TEST_ROOT/"$dir"/compiler.err`;
				errordif=$?
			fi
		fi
		#$cmd &> errors
		if  [[ $errordif -ne 0 ]]; then
			printf "\033[0;31mFAILURE!!!\033[m\n";
			printf "\033[0;33m$ERROR - Error type: $error_type\033[0m\n";
			total_comp_errors=$(($total_comp_errors+1));
			break;
		else
			printf "\033[0;32mSUCCESS!\033[0m\n";
		fi
	done

	if  [[ $error_type -ne 0 ]] ; then
		if  [[ $errordif -ne 0 ]]; then
			printf "\033[0;31mFAILURE: ";
		else
			printf "\033[0;32mSUCCESS: ";
		fi
		printf "HALTING TEST -- Compiler returned error type: %s\033[0m\n" "$error_type";
		continue;
	fi

	linklist=`echo $TEST_ROOT/"$dir"/linker.lst`
	if  [[ -e $linklist ]] ; then
		linkfiles=`cat "$linklist" | tr "\n" " "`
	else
		linkfiles=`ls $TEST_ROOT/"$dir"/*.rsk | tr "\n" " "`
	fi
	linklist=""
	linklist=`printf '%s' "${linkfiles[@]}" | tr "\n" " "`

	cmd=`printf './rx-linker %s rx-runtime.rsk' "$linklist"`
	total_link=$(($total_link+1));
	#done
	#printf "linking    $linklist  \t.......................";
	#printf "linking to $TEST_ROOT/"$dir"/"$dir".e   \t.......................";
	printf "%s%*.*s" "linking   $linklist" 0 $((padlength - ${#linklist}  )) "$pad";

	ERROR=$($cmd 2>&1)
	if  [[ $ERROR != "" ]] ; then
		printf "\033[0;31mFAILURE!!!\033[m\n";
		total_link_errors=$(($total_link_errors+1));
		printf "\033[0;33mLINKER %s\033[0m\n" "$ERROR";
		printf "\033[0;31mHALTING TEST -- Linker failed to link executable\033[0m\n";
		tests=`ls $TEST_ROOT/"$dir"/*.input`
		total_test=$(($total_test+3));
		total_test_errors=$(($total_test_errors+3));
		continue;
	else
		printf "\033[0;32mSUCCESS!\033[0m\n";
	fi


	executable=`ls $TEST_ROOT/"$dir"/"$dir".e`
	tests=`ls $TEST_ROOT/"$dir"/*.input`
	errorcount=0;
	for testin in ${tests[@]}
		do
		testname=`echo "$testin" | cut -f1 -d"."`
		testout=`echo "$testname".out`
		copyout=`echo "$testname".exp`

		#cmd=`printf '%s < %s > %s' "./rx-vm $executable" "$testin" "$testout"`;
		#ERROR=$($cmd 2>&1)
		printf '%s%*.*s' "testing   $testin " 0 $((padlength - ${#testin} - 1 )) "$pad";
		total_test=$(($total_test+1));
		`./rx-vm "$executable" < "$testin" > "$testout"`

		if [[ ! -e $testout ]] ; then
			echo -e "\033[0;31mFAILURE!!!\033[m";
			errorcount=$errorcount+1;
			total_test_errors=$total_test_errors+1;
			continue;
		fi
		#ERROR=$($cmd 2>&1);
		cmd=`diff <(sed 's/at PC=[0-9]*/at PC=0/g' "$testout") <(sed 's/at PC=[0-9]*/at PC=0/g' "$copyout")`
		if  [[ -e $copyout ]] ; then
			#`diff "$testout" "$copyout"`
			ERROR=$($cmd 2>&1);
		else
			`cp $testout $copyout`
		fi
		if  [[ $? -ne 0 ]] ; then
			if [[ $1 == "update" ]] ; then
				echo -e "\033[0;34mUPDATING!!!\033[m";
				`cp $testout $copyout`
			else
				echo -e "\033[0;31mFAILURE!!!\033[m";
				errorcount=$(($errorcount+1));
				total_test_errors=$(($total_test_errors+1));
				echo -e "\033[0;33mDiff error:\n$cmd\033[m";
			fi
		else
			echo -e "\033[0;32mSUCCESS!\033[0m";
		fi
	done

	if  [[ $errorcount -ne 0 ]] ; then
		echo -e "\033[0;31mFAILURE: TEST ENDED WITH $errorcount FAILURES!\033[m!!!";
	else
		echo -e "\033[0;32mSUCCESS: TEST COMPLETED WITHOUT ERRORS!\033[0m";
	fi
done

printf "\n\n\n";
printf "===========================================================================\n";
printf "============================== \033[0;33mSUMMARY\033[m ====================================\n";
printf "===========================================================================\n\n";

printf "Total Compilation Errors: %d/%d\n" "$total_comp_errors" "$total_comp"
printf "Total Linker Errors: %d/%d\n" "$total_link_errors" "$total_link"
printf "Total Test Errors: %d/%d\n" "$total_test_errors" "$total_test"
printf "Your compiler status:\n";
total_score=$(($total_comp*5 +$total_link*2 + $total_test*3))
total_penalty=$(($total_comp_errors*5 +$total_link_errors*2 + $total_test_errors*3))
green=$(( 100 * $(($total_score - $total_penalty)) / $total_score ))
red=$(( 100 * $total_penalty / $total_score ))

statusg=$(printf "%0.1s" "|"{1..100})
statusr=$(printf "%0.1s" "|"{1..100})
printf "[ \033[0;32m%*.*s\033[0;31m%*.*s\033[0m ] " 0 $green $statusg 0 $red $statusr
if [[ $green -gt 95 ]] ; then
	printf "\033[0;32m$green%% -- Excellent!!!\033[0m\n"
elif [[ $green -gt 75 ]] ; then
	printf "\033[0;32m$green%% -- Good!\033[0m\n"
elif [[ $green -gt 40 ]] ; then
	printf "\033[0;33m$green%% -- Needs work...\033[0m\n"
else
	printf "\033[0;31m$green%% -- Critical\033[0m\n"
fi
